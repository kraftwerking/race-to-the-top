//
//  RTSpaceShipView.m
//  Race To The Top
//
//  Created by RJ Militante on 2/11/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "RTSpaceShipView.h"

@implementation RTSpaceShipView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    bezierPath.lineWidth = 2.0;

    
    /* We need a start point for the bezier path so we call the method moveToPoint. Notice that we've made the CGPoint's dynamic based on the view's bounds instead of using magic numbers like 280 or 320. */
    [bezierPath moveToPoint:CGPointMake(1/6.0 * self.bounds.size.width, 1/3.0 * self.bounds.size.height)];
    /* addLineToPoint method creates a straight line from the last bezierPath's point to the current point. */
    [bezierPath addLineToPoint:CGPointMake(1/6.0 *self.bounds.size.width, 2/3.0 *self.bounds.size.height)];
    [bezierPath addLineToPoint:CGPointMake(5/6.0 * self.bounds.size.width, 2/3.0 *self.bounds.size.height)];
    [bezierPath addLineToPoint:CGPointMake(2/3.0 * self.bounds.size.width, 1/2.0 * self.bounds.size.height)];
    [bezierPath addLineToPoint:CGPointMake(1/3.0 * self.bounds.size.width, 1/2.0 * self.bounds.size.height)];
    /* closePath draws a line from the first to the last subpoint */
    [bezierPath closePath];
    /* draws the line defined in the bezierPath object */
    [bezierPath stroke];
    
    /* We can create a CGRect using the method CGRectMake and then add it as a argument for the bezierPathWithRect. This returns a 4 point shape with the specified points in the CGRect. */
    UIBezierPath *cockpitWindowPath = [UIBezierPath bezierPathWithRect:CGRectMake(2/3.0 * self.bounds.size.width, 1/2.0 * self.bounds.size.height, 1/6.0 * self.bounds.size.width, 1/12.0 * self.bounds.size.height)];
    /* Set the fill color to a UIColor so that we will be able to have our shape have a filled color. */
    [[UIColor blueColor] setFill];
    /* Paints the region inside of the UIBezierPath */
    [cockpitWindowPath fill];
}


@end
