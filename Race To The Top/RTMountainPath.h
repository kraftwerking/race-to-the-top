//
//  RTMountainPath.h
//  Race To The Top
//
//  Created by RJ Militante on 2/12/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <CoreGraphics/CGGeometry.h>

@interface RTMountainPath : NSObject

+(NSArray *)mountainPathsForRect:(CGRect)rect;

+(NSArray *)mountainPathsForRect:(CGRect)rect;

+(UIBezierPath *)tapTargetForPath:(UIBezierPath *)path;

@end